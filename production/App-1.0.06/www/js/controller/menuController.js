﻿myApp.controller('menuController', function ($scope, $state, $rootScope, userService, $ionicViewService) {

    $rootScope.showMenu = true;

    $scope.signout = function () {

        userService.signout();
        //localStorage["username"] = "";
        $ionicViewService.nextViewOptions({
            disableBack: true
        });
        $scope.isSignedIn = false;
        $state.go('app.signin', { signout: true });
    };

    //$scope.changeLang = function () {
    //    if ($rootScope.appLanguage == "he") {
    //        localStorage["mvlang"] = "en";
    //        $window.location.reload();
    //    } else {
    //        localStorage["mvlang"] = "he";
    //        $window.location.reload();
    //    }
    //    localize.initLocalizedResources();
    //};

})
