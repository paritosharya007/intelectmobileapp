
myApp.factory('userService', function ($http, $q, $state, $rootScope, loaderService, geolocation) {

    var _data = {
        isSignedIn: false,
        username: null,
        userData: null,
        loading: false,
        email: null,
        passwrodRegexPattern: ""
       
    }

    var _initAuthenticatedUser = function (userData) {
        _data.isSignedIn = true;
        //_data.email = userData.emaiAddress;
        _data.username = userData.username;
        _data.userData = userData;

    }

    var _initUnauthenticatedUser = function () {
        _data.isSignedIn = false;
        _data.username = null;
        _data.userData = null;
    }

    var _signout = function () {
        var deferred = $q.defer();

        _data.loading = true;
        loaderService.show();


        $http.post(globalParams.baseUrl + '/Account/AppLogout', null).
            success(function (data, status, headers, config) {
                if (data && data.Success) {
                    _initUnauthenticatedUser();
                    deferred.resolve();
                }
                else {
                    _initUnauthenticatedUser();
                    deferred.reject();
                }
                _data.loading = false;
                loaderService.hide();

            }).
            error(function (data, status, headers, config) {
                _initUnauthenticatedUser()
                deferred.reject(status);
                _data.loading = false;
                loaderService.hide();

            });
        return deferred.promise;
    };

    var _signin = function (username, password) {
        var deferred = $q.defer();

        _data.loading = true;
        loaderService.show();

        var signinParams = {
            UserName: username,
            Password: password,
            position: geolocation.stringifiedPosition()
        }
        //alert(globalParams.baseUrl);
        //alert(JSON.stringify(signinParams));
        $http.post(globalParams.baseUrl + '/Account/AppLogin', { user: signinParams }).
            success(function (data, status, headers, config) {
                if (data && data.Success) {
                    _initAuthenticatedUser(data.Results);
                    deferred.resolve(data);
                }
                else {
                    _initUnauthenticatedUser(data);
                    deferred.reject(data);
                }
                _data.loading = false;
                loaderService.hide();

            }).
            error(function (data, status, headers, config) {
                //alert(data);
                //alert(status);
                //alert(headers);
                //alert(config);
                _initUnauthenticatedUser()
                deferred.reject(status);
                _data.loading = false;
                loaderService.hide();

            });
        return deferred.promise;
    };

    var _changePassword = function (oldPassword, newPassword) {
        var deferred = $q.defer();

        _data.loading = true;
        loaderService.show();

        var params = {
            oldPassword: oldPassword,
            newPassword: newPassword
        }

        $http.post(globalParams.baseUrl + '/Account/AppChangePassword', params).
            success(function (data, status, headers, config) {
                if (data && data.Success) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data.Results && data.Results.message);
                }
                _data.loading = false;
                loaderService.hide();

            }).
            error(function (data, status, headers, config) {
                deferred.reject(status);
                _data.loading = false;
                loaderService.hide();

            });
        return deferred.promise;
    };

    var _requestSmsToken = function (phoneNumber) {
        var deferred = $q.defer();

        _data.loading = true;
        loaderService.show();

        var params = {
            phoneNumber: phoneNumber
        }

        $http.post(globalParams.baseUrl + '/Account/RequestSmsToken', params).
            success(function (data, status, headers, config) {
                if (data && data.Success) {
                    deferred.resolve(data);
                }
                else {
                    deferred.reject(data.Results && data.Results.message);
                }
                _data.loading = false;
                loaderService.hide();

            }).
            error(function (data, status, headers, config) {
                deferred.reject(status);
                _data.loading = false;
                loaderService.hide();

            });
        return deferred.promise;
    };

    var _validateSmsToken = function (phoneNumber, smsToken) {
        var deferred = $q.defer();

        _data.loading = true;
        loaderService.show();

        var params = {
            phoneNumber: phoneNumber,
            smsToken: smsToken
        }

        $http.post(globalParams.baseUrl + '/Account/AppValidateSmsToken', params).
            success(function (data, status, headers, config) {
                if (data && data.Success) {
                    _initAuthenticatedUser(data.Results);
                    deferred.resolve(data);
                }
                else {
                    _initUnauthenticatedUser(data);
                    deferred.reject(data);
                }
                _data.loading = false;
                loaderService.hide();

            }).
            error(function (data, status, headers, config) {
                _initUnauthenticatedUser()
                deferred.reject(status);
                _data.loading = false;
                loaderService.hide();

            });
        return deferred.promise;
    };



    var _getUser = function () {
        var deferred = $q.defer();
        _data.loading = true;

        $http.get(globalParams.baseUrl + '/Account/AppGetUser').
            success(function (data, status, headers, config) {
                if (data && data.Success) {
                    _initAuthenticatedUser(data.Results);
                    deferred.resolve(data);
                }
                else {
                    _initUnauthenticatedUser(data);
                    deferred.reject(data);
                }
                _data.loading = false;
                loaderService.hide();

            }).
            error(function (data, status, headers, config) {
                _initUnauthenticatedUser()
                deferred.reject(status);
                _data.loading = false;
                loaderService.hide();

            });
        return deferred.promise;
    };


    return {
        data: _data,
        getUser: _getUser,
        signout: _signout,
        signin: _signin,
        changePassword: _changePassword,
        requestSmsToken: _requestSmsToken,
        validateSmsToken: _validateSmsToken

    }
});