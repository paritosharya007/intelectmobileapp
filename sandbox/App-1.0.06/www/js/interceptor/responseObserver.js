﻿myApp.factory('responseObserver',
    function responseObserver($q, $window, $injector) {
        return function (promise) {
            return promise.then(
                function (successResponse) {
                    return successResponse;
                }, function (errorResponse) {
                    switch (errorResponse.status) {
                        case 403:
                            $injector.get('$state').go('app.changePassword', {passwordExpired:true});
                            break;
                        case 500:
                            $window.location = './500.html';
                    }

                    return $q.reject(errorResponse);
                });
        };
    });